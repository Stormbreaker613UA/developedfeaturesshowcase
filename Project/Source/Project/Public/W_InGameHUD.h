// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "W_InGameHUD.generated.h"

/**
 * 
 */
UCLASS()
class PROJECT_API UW_InGameHUD : public UUserWidget
{
	GENERATED_BODY()
	
};
